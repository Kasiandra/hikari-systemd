This service depends on lightdm.service

```
$ chmod +x ~/.config/systemd/user/hikari-for-wayland-display.sh 
$ systemctl --user enable waybar.service
$ systemctl --user enable hikari.service
$ systemctl --user daemon-reload
```